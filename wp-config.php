<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'authenticweb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')b,0#(2D=V3z;<kGMOj5vndduhBN2~89.Fh y;|+3>u;mT.>:sE^cGS<&6.P&2kq');
define('SECURE_AUTH_KEY',  'o%g,0P|`RC~O~F/50tjkK@c{w+(qE0{udq-z!Naw+^p+H-$+.:ddgI`KC{R092;}');
define('LOGGED_IN_KEY',    '`{!uz^jnb<C<g=PW->Ba?T?MgH~D:<*=25-E;;P,C#AB(84@{;-#|JoLE+OX,=Mo');
define('NONCE_KEY',        'Vsjd|G&`kf0fV5QE1,@zj;-A2np)->0O*$eZ] -ZyQxj{]<<f9:P[m^@[0l)`P_d');
define('AUTH_SALT',        'qCp~Oy1X{-V&zd_#xrYr0BCv0e(2Fb31(%kUoJDn<H V=}*U3|ruo$D9*OXH?ZNB');
define('SECURE_AUTH_SALT', 'gnXU<P`w_56,to}+(R<6ahf(_Y|{2,0qZ3s1Qg|+qjr#-kH$|_H3cxtfp;[=9,*D');
define('LOGGED_IN_SALT',   '#^Pyiw3+U:?>+s[V[cTJ,.i&5IYda0(V5j%7(Bo^~6Ou$sjDN?g6NJm($Zyr$.]q');
define('NONCE_SALT',       'R>{AeIS/-jXx6W#Hx*p8T=!#)L(mVZN(v=RhW;Lw+TQb,|Qm;u1KPL-m[/Is,0nX');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
