<?php
/**
 * Template Name: Blog, with Sidebar
 *
 * @package WordPress
 * @subpackage Continental Hair
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<section class="site-content">

		<div class="container" id="content" role="main">

			<?php query_posts('post_type=post&post_status=publish&offset=2&posts_per_page=10&paged='. get_query_var('paged')); ?>

			<?php if( have_posts() ): ?>
				<?php while( have_posts() ): the_post(); ?>
				<div class="row post">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="post_date">
							<span class="orange"><?php echo get_the_date(); ?></span>
							<span class="author">Posted by: <b><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta('last_name'); ?> </b></span>
						</div>
					</div>
					<div id="post-<?php get_the_ID(); ?>" <?php post_class(); ?>>
						<div class="col-xs-12 col-sm-3 col-md-3">
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(200,220)); ?></a>
						</div>
						<div class="col-xs-12 col-sm-9 col-md-9">
							
							<h1 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
							
							
							<?php the_excerpt(); ?>
						</div>
					</div>
				</div>
				<?php endwhile; ?>

			<div class="navigation">
			  <span class="newer"><?php previous_posts_link(__('« Newer','example')) ?></span> <span class="older"><?php next_posts_link(__('Older »','example')) ?></span>
			</div><!-- /.navigation -->

			<?php endif; wp_reset_query(); ?>
		</div><!-- #content -->
	</div><!-- #primary -->
<?php get_footer(); ?>
