<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?>
<div class="container">
	<div class="row">
		
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="post_date">
						<span class="orange"><?php echo get_the_date(); ?></span>
						<span class="author">Posted by: <b><?php echo get_the_author_meta( 'first_name' ); ?> <?php echo get_the_author_meta('last_name'); ?> </b></span>
					</div>
				</div>
			<div class="col-md-4">
				<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
				<div class="entry-thumbnail">
					<?php the_post_thumbnail(); ?>
				</div>
				<?php endif; ?>
			</div>
			<div class="col-md-8">
				
				<?php if ( is_single() ) : ?>
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<a href="https://twitter.com/authenticwebinc" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @authenticwebinc</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<?php else : ?>
				<h1 class="entry-title">
					<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>

				</h1>
				<a href="https://twitter.com/authenticwebinc" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @authenticwebinc</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
				<?php endif; // is_single() ?>

				<div class="entry-meta">
					<?php //twentythirteen_entry_meta(); ?>
				</div><!-- .entry-meta -->

				<?php if ( is_search() ) : // Only display Excerpts for Search ?>
				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div><!-- .entry-summary -->
				<?php else : ?>
				<div class="entry-content">
					<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
				</div><!-- .entry-content -->
				<?php endif; ?>
			</div>
			
		</article><!-- #post -->
	</div>
</div>
