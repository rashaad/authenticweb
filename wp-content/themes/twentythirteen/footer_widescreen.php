<footer class="widescreen">
	<div class="container">
		<!--
		<div class="row">
			<div class="col-md-4">
				<ul>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<ul>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
				</ul>
			</div>
			<div class="col-md-4">
				<ul>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
					<li><a href="#">link 1</a></li>
				</ul>
			</div>
		</div>
		-->
		<div class="row">
			<div class="col-md-6">
				<p><small>Authentic Web Inc<sup>TM</sup> © All rights reserved 2013.  <a data-toggle="modal" href="#PrivacyPolicy">Privacy Policy</a></small></p>
			</div>
			
			<div class="col-md-6 right-align-text">
				<p>Contact Us for a <b>FREE</b> consultation <b>1-855-436-8853</b></p>
			</div>	
		</div>
	</div>
</footer>