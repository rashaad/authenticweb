<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	
	<a id="home"></a>
	<section id="home">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-12">
					<h1>TAKE CONTROL OF YOUR <b>.BRAND</b> REGISTRY</h1>
					<hr>
					<h3>We remove the complexity from closed registry management.</h3>
				</div>
				<div class="col-md-4 col-sm-6">
					<img src="<?php bloginfo('template_directory'); ?>/img/stickguy_arrows.png" width="100%" class="stickguy"/>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="row">
						<div class="col-md-5">
							<h3 style="margin-bottom:7px;">Digital Innovation </h3>
							<h4 style="margin:0; margin-bottom:7px;">and the</h4>

							<h3 class="no-margin">Brand Registry</h3>
						</div>
						<div class="col-md-7">
							<img class="tip" src="<?php bloginfo('template_directory'); ?>/img/webdude_graph2.jpg" width="100%"/>
						</div>
					</div>
					<a href="http://momentumevents.com/gtldny/webinar3/" style="text-decoration:none;" target="_blank"><button type="button" class="btn btn-primary orange-bg btn-lg btn-block" data-toggle="tooltip" data-placement="left">Attend the Webinar on Jan 16.14</button></a>
				</div>
			</div>
			<br>
			<hr>
			<div class="row">

				<div class="col-md-4">
					<h2><img src="<?php bloginfo('template_directory'); ?>/img/eyeicon.png">Visibility</h2>
					<h4>Complete visibility into your global <b>.Brand</b> domain portfolio.</h4>
				</div>
				<div class="col-md-4">
					<h2><img src="<?php bloginfo('template_directory'); ?>/img/gearsicon.png">Tools</h2>
					<h4>Powerful and effective tools to create, approve and administer <b>.Brand</b> domains.</h4>
				</div>
				<div class="col-md-4">
					<h2><img src="<?php bloginfo('template_directory'); ?>/img/graphicon.png">Dashboard</h2>
					<h4>Real-time dashboard and reporting tools to monitor domain activity.</h4>
				</div>

			</div>

		</div>
		<?php include 'footer_widescreen.php'; ?>		
	</section>

	<a id="ebook"></a>
	<section id="ebook">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>Ebook</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-8">
					<h1>What is <b>.BrandIQ?</b> What is Yours?</h1>
				</div>
				<div class="col-md-4 social-icons">
					<?php dd_twitter_generate('Normal','authenticweb') ?>
					<?php dd_google1_generate('Normal') ?>
					<?php dd_linkedin_generate('Normal') ?>
				</div>
				
			</div>

			<hr>

			<div class="row">
				<div class="col-md-4">
					<img src="<?php bloginfo('template_directory'); ?>/img/ebook-brandiq.jpg" width="100%"/>
				</div>

				<div class="col-md-5">
					<p>In this ebook, learn about <b>.BrandIQ</b> (DOTBRANDIQ). See how a high <b>.BrandIQ</b> indicates understanding of new registry capabilities and a plan to launch innovations. Learn what motivated brands to apply and where each sits on the <b>.BrandIQ</b> curve. We plot these organizational "personas". It will help identify your current curve position and your desired position down the road.
					<br><br>
					Having a high <b>.BrandIQ</b> indicates an organizational understanding of the capabilities inherent in a closed registry and how it maps those capabilities to strategic priorities in the form of use cases. High IQ also indicates a well formed GoToMarket plan.
					</p>
				</div>

				<div class="col-md-3 centered">
					<button type="button" class="btn btn-primary orange-bg btn-lg btn-block" data-toggle="modal" data-target="#freeEbook">GET YOUR EBOOK</button>
					<hr>
					<img src="<?php bloginfo('template_directory'); ?>/img/ebook2.jpg" width="100%" class="ebook2">
				</div>
			</div>

		</div>
		<?php include 'footer_widescreen.php'; ?>						
	</section>

	<a id="services"></a>
	<section id="services">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>Services</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<img src="<?php bloginfo('template_directory'); ?>/img/shouting-guy.png" width="100%"/>
				</div>

				<div class="col-md-8">
					<p>Authentic Web offers a robust platform for brands operating closed registries. It gives brands control and visibility to efficiently manage a global closed domain portfolio.</p>

					<div class="row">
						<div class="col-md-6">
							<img src="<?php bloginfo('template_directory'); ?>/img/bram-service.jpg" width="100%">
						</div>
						<div class="col-md-6">
							<h2>Function and Benefit</h2>
							<p>BRAM delivers the critical functions you need to manage your .brand registry.
							<br><br>
							Our product, Brand Registry Asset Manager(BRAM™), is an enterprise management platform that handles registry names within a heirarchical structure, role-based permissioning, workflow, case and task management, messaging, custom DNS and other critical technology automation functions.
							<br><br>
							BRAM is different than traditional domain name systems. It is simpler and designed for a brand's internal use. As you roll out a closed brand registry, BRAM has the tools needed to publish and deliver engagement experiences on .brand names and networks.
							<br><br>
							BRAM removes the complexity from closed domain management, enabling stakeholders to take control of their registry.</p>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<img src="<?php bloginfo('template_directory'); ?>/img/services-02.jpg" width="100%"/>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<hr>
							<br><br>
							<h2>Turnkey</h2>
							<p>
								Authentic Web takes the complexity elements of operating a registry off your hands both through systems and Professional Services. Our team has worked in the name space since ICANN's creation in the late 1990s. We work with your back-end registry provider, operate as your Accredited Registrar as a turnkey brand solution to handle the registrar compliance and reporting requirements of ICANN.
								<br><br>
								We can also integrate to your current registrar partner system and provide the front-end BRAM tools required by brand operating entities, departments and teams.
							</p>
							<br>
							<hr>
							<br>
							<h2>Stakeholders value</h2>
							<p>
								The platform gives stakeholders what they need. It is designed to address the various organizational challenges in managing something as complex as a brand registry. We give you control, visibility and an efficient way to minimize the technical and administrative burdens.
							</p>

							<table class="table table-condensed">
								<thead>
									<tr>
										<th>CORPORATE</th>
										<th>EXECUTIVES</th>
										<th>IT</th>
										<th>TEAMS</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Centralized Control</td>
										<td>Visibility</td>
										<td>Configuration Control</td>
										<td>Execution Empowerment</td>
									</tr>
									<tr>
										<td>Distributed Operations</td>
										<td>Team Execution Empowerment</td>
										<td>Technical Automation</td>
										<td>Easy to use</td>
									</tr>
									<tr>
										<td>Turnkey Outsource</td>
										<td class="emptytablebox"></td>
										<td>Restricted Access</td>
										<td class="emptytablebox"></td>
									</tr>
								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>

		</div>
		<?php include 'footer_widescreen.php'; ?>			
	</section>

	<a id="technology"></a>
	<section id="technology">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>Technology</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<img src="<?php bloginfo('template_directory'); ?>/img/handshake.jpg" width="100%"/>
					<hr>
					<p>Interested to discuss a partner relationship to support our client's missions?</p>
					<button type="button" class="btn btn-primary orange-bg btn-lg btn-block" data-toggle="modal" data-target="#Inquire">INQUIRE</button>
					<hr>
					<?php // if ( dynamic_sidebar('News Widget Area') ) : else : endif; ?>
				</div>
				<div class="col-md-8">
					<h2>Technology and Partners</h2>
					<p>Authentic Web partners and integrates only with top-tier technologies and providers.
					<br><br>
					Our purpose built SaaS platform is structured on leading edge open technologies to ensure flexibility and extensibility for future innovation requirements. The BRAM system is a robust and flexible platform, configurable to your organization's domain governance policties, organizational structure as well as approval, provisioning workflow and automation requirements.
					</p>

					<div class="indented">
						<h3><span class="glyphicon glyphicon-stats"></span>&nbsp;&nbsp;Performance and Scalability</h3>
						<p>Utilizing best-in-class infrastructure with Amazon Web Services, the integrated system performance and scalability is structured to deliver under anyload, anywhere at anytime.</p>
						<hr>

						<h3><span class="glyphicon glyphicon-random"></span>&nbsp;&nbsp;Integration</h3>
						<p>Automated domain provisioning with integration capabilities to third party registrar or back-end registry systems and other best of breed web technologies such as top-tier CMS systems and other digital engagement technologies, the Authentic Web system bridges the domain portfolio management space to digital experience creation.</p>
						<hr>

						<h3><span class="glyphicon glyphicon-lock"></span>&nbsp;&nbsp;Security</h3>
						<p>Strict adherence to the security of your company' domain assets, coupled with integration to top-tear malware options and DNS security services ensure the life-long safety of your domain assets. Security is a prime focus. You will know your domain portfolio is protected through a best in class system security structures and protocols.</p>
					</div>
					<hr>

					<div class="row">
						<div class="col-md-4">
							<img src="<?php bloginfo('template_directory'); ?>/img/amazon-partners.jpg">
						</div>
						<div class="col-md-4">
							<img src="<?php bloginfo('template_directory'); ?>/img/opensrs-partners.jpg">
						</div>
						<div class="col-md-4">
							<img src="<?php bloginfo('template_directory'); ?>/img/tucows-partners.jpg">
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4">
							<img src="<?php bloginfo('template_directory'); ?>/img/php-partners.jpg">
						</div>
						<div class="col-md-4">
							<img src="<?php bloginfo('template_directory'); ?>/img/symfony-partners.jpg">
						</div>
					</div>

				</div>
			</div>
		</div>
		<?php include 'footer_widescreen.php'; ?>
	</section>

	<a id="blog"></a>
	<section id="blog">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>Blog</h1>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8">
					<?php query_posts('post_type=post&cat=-3&post_status=publish&posts_per_page=2&paged=' . get_query_var('paged')); ?>
					<?php if (have_posts()):?>
						<?php while( have_posts() ): the_post(); ?>
							<div class="row">
								<div class="col-md-4">
									<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(200,220)); ?></a>
								</div>

								<div class="col-md-8">
									<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
									<small><em><?php the_time('F jS, Y');?></em></small>
									<br><br>
									<?php the_excerpt(); ?>
									<a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary orange-bg">read more</button></a>
								</div>
							</div>

							<hr>

						<?php endwhile;?>
						
						<span class="older_posts"><a href="/blog">Older Posts</a></span>
					<?php endif; wp_reset_query(); ?>				
				</div>

				<div class="col-md-4">

					<img src="<?php bloginfo('template_directory'); ?>/img/stickguy-news.jpg">
					<hr>
					<a href="https://twitter.com/authenticwebinc" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @authenticwebinc</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
					<?php if ( dynamic_sidebar('Secondary Widget Area') ) : else : endif; ?>
				</div>
			</div>
		</div>
		<?php include 'footer_widescreen.php'; ?>
	</section>

	<a id="about"></a>
	<section id="about">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>About</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-8">
					<h2>Company</h2>
					<p>Authentic Web is a platform services company. The platform gives brands a feature-rich way to control and manage their top-level domain registries. In real-time, brands have complete visibility into the status of all their registry domains, displayed using a user-friendly dashboard and reporting tools. We give brands the power to control their domain portfolios – everything from domain requests, approvals, provisioning and reporting to role based permissioning and technical automation functions. For brands that need to take control of their domain registry, Authentic Web is a cost-efficient, cloud-based platform that helps make sense of the fast-changing ecosystem. Authentic Web is based in Toronto, Canada.</p>
					<hr>
					<h2>Team</h2>
					<div class="row">
						
						<div class="col-md-12">
							<div class="col-md-3">
								<img src="<?php bloginfo('template_directory'); ?>/img/peter-lamantia.jpg" class="img-thumbnail">
							</div>
							<div class="col-md-9">
								<p>
									<?php the_author_meta( 'description', 3 ); ?> 
								</p>
								<a href="http://ca.linkedin.com/in/plamantia/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/linkedin-link.jpg"></a>
							</div>
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-3">
								<img src="<?php bloginfo('template_directory'); ?>/img/bill-keenan.jpg" class="img-thumbnail">
							</div>
							<div class="col-md-9">
								<p>
									<?php the_author_meta( 'description', 2 ); ?> 
								</p>
								<a href="http://ca.linkedin.com/in/wgkeenan" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/linkedin-link.jpg"></a>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<img src="<?php bloginfo('template_directory'); ?>/img/stickguys.jpg">

					<hr>
					<center>
					<a href="https://twitter.com/authenticwebinc" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @authenticwebinc</a>
					</center>
					<hr>
					<?php if ( dynamic_sidebar('News Widget Area') ) : else : endif; ?>

				</div>
			</div>
		</div>
		<?php include 'footer_widescreen.php'; ?>
	</section>

	<a id="contact"></a>
	<section id="contact">
		<div class="container">
			<div class="row page-heading">
				<div class="col-md-12">
					<h1>Contact Us</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-4">
					<img src="<?php bloginfo('template_directory'); ?>/img/stickguy-mail.jpg">
				</div>
				<div class="col-md-4">
					<?php echo do_shortcode('[contact-form-7 id="13" title="Contact form 1"]'); ?>
				</div>
				<div class="col-md-4">
					<p>
						We'd be happy to provide you with more information about our .brand registry management platform.
						<br><br>
						Take control of your brand registry. Fill out the form, call or email us and we will show you how.
						<br><br>
						Authentic Web Inc. <br><br>
						<b>Email:</b> <a href="mailto: info@authenticweb.com">info@authenticweb.com</a>
						<hr>
						<b>Toll Free North America:</b> 1-855-436-8853
						<hr>
						<b>Address:</b> 219 Dufferin Street, Toronto ON, M6K 3J1
						<hr>
						<a href="https://twitter.com/authenticwebinc" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @authenticwebinc</a>
					</p>
					<hr>
					<iframe width="100%" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.ca/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=219+Dufferin+Street,+Toronto+Ontario+M6K+3J1&amp;aq=&amp;sll=49.891235,-97.15369&amp;sspn=47.833715,79.013672&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=219+Dufferin+St,+Toronto,+Ontario+M6K+1Y9&amp;ll=43.636931,-79.426518&amp;spn=0.006514,0.009645&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="https://www.google.ca/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=219+Dufferin+Street,+Toronto+Ontario+M6K+3J1&amp;aq=&amp;sll=49.891235,-97.15369&amp;sspn=47.833715,79.013672&amp;t=m&amp;ie=UTF8&amp;hq=&amp;hnear=219+Dufferin+St,+Toronto,+Ontario+M6K+1Y9&amp;ll=43.636931,-79.426518&amp;spn=0.006514,0.009645&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>
				</div>
			</div>
		</div>
		<?php include 'footer_normal.php'; ?>
	</section>

<?php get_footer(); ?>

<!-- Modal -->
  <div class="modal fade" id="freeEbook" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height: 450px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">eBook Download</h4>
        </div>
        <div class="modal-body" style="height:100%;">
           <style type="text/css">
			    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif;  width:500px;}
			    #mc-embedded-subscribe {
			        clear: none;
			        margin: 0;
			    }
			    #mc_embed_signup input {
			        width: 100%;
			    }
			    #mc_embed_signup .mc-field-group {
			        width: 100%;
			    }
			    .mc-field-group {
			    	margin-bottom: 20px;
			    }

			    /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
			       We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style>
			<div id="mc_embed_signup">
			<form action="http://authenticweb.us7.list-manage.com/subscribe/post?u=c631a5dd740b92ee9115db538&amp;id=5ec19f6b8c" method="post" id="mc-embedded-subscribe-form" name="mcsub" class="validate" target="_blank" novalidate>
			    
			<div class="mc-field-group">
			    <label for="mce-EMAIL">Email Address </label>
			    <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
			</div>
			<div class="mc-field-group">
			    <label for="mce-FNAME">First Name </label>
			    <input type="text" value="" name="FNAME" class="form-control" id="mce-FNAME">
			</div>
			<div class="mc-field-group">
			    <label for="mce-MMERGE3">Phone </label>
			    <input type="text" value="" name="MMERGE3" class="form-control" id="mce-MMERGE3">
			</div>
			<div class="mc-field-group">
			    <label for="mce-COMPANY">Company </label>
			    <input type="text" value="" name="COMPANY" class="form-control" id="mce-COMPANY">
			</div>
			    <div id="mce-responses" class="clear">
			        <div class="response" id="mce-error-response" style="display:none"></div>
			        <div class="response" id="mce-success-response" style="display:none"></div>
			    </div>  

			    <div class="submit_container">
			<input type="submit" id="mc-embedded-subscribe" class="btn btn-primary btn-lg btn-block" style="border: none" name="submit" value="Subscribe for eBook">


			</div>

			</form>
			<br>

			</div>
        </div>
        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->


<!-- Modal -->
  <div class="modal fade" id="PrivacyPolicy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height: 400px;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Privacy Policy</h4>
        </div>
        <div class="modal-body" style="height:100%;">
           <iframe style="height: 300px;width:100%;border: none;" src="http://authenticweb.rocketfuse.com/wordpress/privacypolicy.html">
			  <p>Your browser does not support iframes.</p>
			</iframe>
        </div>
        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

<div id="top" style="display:none;"> 
       <a tabindex="-1" href="#" id="metadata-link" data-target="#modal" data-toggle="modal">Metadata</a>
    </div>

   <div id="modal" class="modal hide fade in" style="display:none;">
      <div class="modal-header">header<a class="close" data-dismiss="modal">x</a></div>
      <div class="modal-body"></div>
      <div class="modal-footer">footer</div>
   </div>

<!-- Modal -->
  <div class="modal fade" id="Inquire" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height: auto;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Contact us with your interest</h4>
        </div>
        <div class="modal-body" style="height:100%;">
           	<form id="Inquire" class="form-horizontal" role="form">

           	  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label"><b>First*</b></label>
			    <div class="col-lg-10">
			      <input data-required="true" class="form-control" id="FirstName" placeholder="John">
			    </div>
			  </div>

           	  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label"><b>Last*</b></label>
			    <div class="col-lg-10">
			      <input data-required="true" class="form-control" id="LastName" placeholder="Smith">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label">Title</label>
			    <div class="col-lg-10">
			      <input class="form-control" id="Title" placeholder="Vice President/Director/Manager">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label">Company</label>
			    <div class="col-lg-10">
			      <input class="form-control" id="Company" placeholder="Authentic Web">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label">Website</label>
			    <div class="col-lg-10">
			      <input class="form-control" id="Website" placeholder="http://www.authenticweb.com">
			    </div>
			  </div>
			  

			  <div class="form-group">
			    <label for="inputEmail1" class="col-lg-2 control-label"><b>Email*</b></label>
			    <div class="col-lg-10">
			      <input data-required="true" type="email" data-type="email" class="form-control" id="Email" placeholder="john@authenticweb.com">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label">Phone</label>
			    <div class="col-lg-10">
			      <input data-type="phone" class="form-control" id="Phone" placeholder="888-888-8888">
			    </div>
			  </div>

			  <div class="form-group">
			    <label for="inputPassword1" class="col-lg-2 control-label">Message</label>
			    <div class="col-lg-10">
			      <textarea id="Message" class="form-control" rows="3"></textarea>
			    </div>
			  </div>
			  
			  
			  <div class="form-group">
			    <div class="col-lg-offset-2 col-lg-10">
			      <button type="submit" onClick="checkscript();" class="btn btn-default">Submit</button>
			    </div>
			  </div>
			</form>
           
        </div>
        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

 <!-- Modal -->
  <div class="modal fade" id="successForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height: auto;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Contact us with your interest</h4>
        </div>
        <div class="modal-body" style="height:100%;">
           	<span class="green">SUCCESS!</span> Thank you for your inquiry. We will review and be in touch
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="NotificationModal" data-dismiss="modal">Close</button>
        </div>
        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <!-- Modal -->
  <div class="modal fade" id="FailForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="height: auto;">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Contact us with your interest</h4>
        </div>
        <div class="modal-body" style="height:100%;">
           	Sorry, the form was not filled correctly.
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="NotificationModal" data-dismiss="modal">Close</button>
        </div>

        
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

 <div id="popover_content_wrapper" style="display:none;">
 	<div style="font-size:12px; width: 300px; text-align: left;">Learn about digital innovation opportunities, resulting from new capabilities of a brand registry.</div>
    <div style="text-align: left; font-size: 12px; margin-top: 15px;">
    	<table id="webinar-popup">
    		<tr>
    			<td><span class="glyphicon glyphicon-ok"></span></td>
    			<td class="content">Assess your organization’s (dot) BrandIQ.</td>
    		</tr>
    		<tr>
    			<td><span class="glyphicon glyphicon-ok"></span></td>
    			<td class="content">Think about use cases to deliver on digital engagement and brand building priorities.</td>
    		</tr>
    		<tr>
    			<td><span class="glyphicon glyphicon-ok"></span></td>
    			<td class="content">Address challenges with a practical plan to deploy and grow your brand ecosystem.</td>
    		</tr>
    		<tr>
    			<td><span class="glyphicon glyphicon-ok"></span></td>
    			<td class="content">Gain competitive advantage by differentiating with registry strategies.</td>
    		</tr>
    	</table>
    	
	</div>
 </div>

<script>
	function isValidEmailAddress(emailAddress) {
	    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	    return pattern.test(emailAddress);
	};

	function checkscript() {
		// here we'll do some validation
		//var formvalid = $( '#Inquire' ).parsley( 'isValid' );
		formValid = false;
		var Firstname = $('#FirstName').val();
		var Lastname = $('#LastName').val();
		var Email = $('#Email').val();
		var Title = $('#Title').val();
		var Company = $('#Company').val();
		var Website = $('#Website').val();
		var Phone = $('#Phone').val();
		var Message = $('#Message').val();


		if (Firstname.length > 0 && Lastname.length > 0 && isValidEmailAddress(Email)) {
			formValid = true;
		}


		if (formValid) {

			// here we'll submit this form
			$.ajax({
			  type: "POST",
			  url: "inquireformsubmit.php",
			  data: { firstname: Firstname, lastname: Lastname, title: Title, company: Company, website: Website, email: Email, phone: Phone, message: Message }
			}).done(function( msg ) {
				//alert( "Data Saved: " + msg );
				// popup modal here..
				$('#successForm').modal()
				$('#Inquire').modal('hide');
			});
			
		} else {
			$('#FailForm').modal()
		}
	}

    $("#mc-embedded-subscribe").click(function(){
        $('#freeEbook').modal('hide');
    });

    $(function() {
    	$(".tip").tooltip({ 
	        html: true, 
	        title: function() {
	          return $('#popover_content_wrapper').html();
	        },
	        placement: 'bottom'
    	});
    	/*
    	if (window.innerWidth > 990) {
	    	$(".tip").tooltip({ 
		        html: true, 
		        title: function() {
		          return $('#popover_content_wrapper').html();
		        }
	    	});
	    } else {
	    	$(".tip").tooltip({ 
		        html: true, 
		        title: function() {
		          return $('#popover_content_wrapper').html();
		        }
	    	});
	    }
		*/
    });


</script>

<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-44339187-1', 'authenticweb.com');
	ga('send', 'pageview');
</script>
