<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
	<section style="padding-top:0;margin-top:0;">
		<div id="primary" class="content-area">
			<div id="content" class="site-content" role="main">

				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>

					<div class="container">
						<div class="row">
							<div class="col-md-9">
								<?php get_template_part( 'content', get_post_format() ); ?>
								
								<?php //twentythirteen_post_nav(); ?>
							</br>
							
								<?php comments_template(); ?>
							</div>
							<div class="col-md-3">
								<article>
									<?php dd_twitter_generate('Normal','authenticweb') ?>
									<?php dd_google1_generate('Normal') ?>
									<?php dd_linkedin_generate('Normal') ?>
									<?php if ( dynamic_sidebar('Main Widget Area') ) : else : endif; ?>
								</article>
							</div>
						</div>

					</div>

				<?php endwhile; ?>

			</div><!-- #content -->
		</div><!-- #primary -->
	</section>

<?php get_footer(); ?>